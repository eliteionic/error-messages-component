import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public myForm: FormGroup;
  public usernameErrors: Record<string, string> = {
    required: 'You... kind of need a username',
  };

  constructor(private fb: FormBuilder) {
    this.myForm = this.fb.group({
      username: ['', Validators.required],
      email: ['', Validators.required],
      message: ['', Validators.required],
    });
  }
}
